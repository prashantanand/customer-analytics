# Customer Analytics using Probability Models

This project's objective to implement the predictive models in the field of Customer Analytics as an Excel Add-In. The models implemented in this project are based on the work of Bruce G.S. Hardie and Peter S. Fader and documented at [Applied Probability Models in Marketing Research](http://www.brucehardie.com/talks/supp_mats00.pdf). This effort started as an academic project supervised by [Dr. Arun Gopalakrishnan](https://olin.wustl.edu/EN-US/Faculty-Research/Faculty/Pages/FacultyDetail.aspx?username=agopala) at Washington University, St. Louis. Currently on the NBD model is implemented under this project but it is hoped that more models would be added in the future.

The project is written in C# and uses the following libraries:

1. [Microsoft Solver Foundation](https://msdn.microsoft.com/en-us/library/ff524497(v=vs.93).aspx) - Used for optimization when building the model.
2. [Excel DNA](https://excel-dna.net/) - Used to build the Excel Add-In.

## Usage
The excel add-in published in the [Excel Add-In](https://gitlab.com/prashantanand/customer-analytics/tree/master/Excel%20Add-Ins) folder can be downloaded and added to Excel. Video instructions on how to install a Excel Add-In can be found [here](https://www.youtube.com/watch?v=reuU2zUsEPM).

Once the add-in is added you will see "Customer Analytics" section on the ribbon. 

![alt text](images/image1.png)

To enter the data for training a model, click on the Setup button. Which will enter the headers for the expected data. After that, enter the data needs to be trained.

![alt text](images/image2.png)

Once the data is entered the "Train" button needs to be clicked to generate the model. Along with the model, the expected values and other relevant results are added.

![alt text](images/image3.png)

## Output Interpretation for the NBD Models.

The output of the training of the model is added to the sheet at the end of training. The output has the following parts:

1. Model - Describes the shape(r) and size (alpha) of the distribution and the p-value indicating the confidence of the current model.
2. Expected - Predicted values based on the created model.
3. Probability - Calculated proabability of each of the counts happening.
4. LogLikelihood - The measure of deviation of the expected value from the observed values. 

## Note
1. The occurence input for the NBD models can be given as a range. The occurences are required to be contigous. If an occurence has missing data, the count needs to be set as 0 for the occurence.

    ![alt text](images/image4.png)

2. The Microsoft Solver Foundation Library has severe limitations when applying to probability models that use Beta, Gamma and other probability distribution functions. The modeling language is limited to basic mathematical functions. To be able to use it for generating probability models, this limitation needs to be extended.

## Alternative Approaches
Other approaches were explored to implement the probability models as Excel add-ins as part of this projects. In this section we briefy describe them.

1. Implementing the models in Python was an obvious choice because of the extensive set of data science related libraries available for the language. Availability of optimization libraries and advanced mathematical functions makes implementing probability models comparatively easier than the approach we took for this project. A successful implementation of BG-NBD Model using python can be seen as part of the article, [BG-NBD Model for Customer Base Analysis, by Ben Keen](http://benalexkeen.com/bg-nbd-model-for-customer-base-analysis-in-python/). However, one of the main goals of this project was to make these models more easier to use for practitioners by building an Excel Add-In. The [xlwings library](https://www.xlwings.org/) showed a lot of promise in this regard. However, the machines running these add-ins produced using this library still needed python installed on the machine and additional python libraries installed. 

2. The self contained excel add-in created by using C# along with Excel DNA libraries made the deployment and distribution of the add-ins quite easy. But in order to write these models in C# we had very limited choice of optimization libraries that were free for use for academic purposes. We attempted  to adopt some open source optimiation code along with [Math.Net Numerics libary](https://numerics.mathdotnet.com/functions.html)(for distribution functions). However we did not observe the level of accuracy that can be achieved by building the models completely in Excel.