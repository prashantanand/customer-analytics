﻿using Microsoft.SolverFoundation.Services;



namespace CustomerAnalytics.Core.Math
{
  public class Functions
  {
    //Gamma233123

    private static readonly int g = 7;
    private static readonly double[] p = {0.99999999999980993, 676.5203681218851, -1259.1392167224028,
       771.32342877765313, -176.61502916214059, 12.507343278686905,
       -0.13857109526572012, 9.9843695780195716e-6, 1.5056327351493116e-7};

    public static Term Gamma(Term z)
    {
      Term smallZ = System.Math.PI / (Model.Sin(System.Math.PI * z) * Gamma(1 - z));

      return Model.If(z <= 0.5, smallZ, GetLargeZ(z));
    }

    private static Term GetLargeZ(Term z)
    {
      z -= 1;
      Term x = p[0];
      for (int i = 1; i < g + 2; i++)
      {
        x += p[i] / (z + i);
      }
      Term t = z + g + 0.5;
      return Model.Sqrt(2 * System.Math.PI) * (Model.Power(t, z + 0.5)) * Model.Exp(-t) * x;

    }

    //Factorial

    public static Term Factorial(Term x)
    {
      Term result = 1;

      Model.ForEach(new Set(1, x, 1), (y) => result = result * y );

      return result;
    }


  }
}
