﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerAnalytics.Core.NBD
{
  public class NBDModel: IModel
  {
    public double R { get; set; }
    public double Alpha { get; set; }
  }
}
