﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace CustomerAnalytics.Core
{
    public interface ISolver
    {
        Dictionary<string,double> Train(IEnumerable<ITrainingData> input);
    }
}
