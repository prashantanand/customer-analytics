﻿using Microsoft.SolverFoundation.Services;
using System.Collections.Generic;
using System.Linq;

namespace CustomerAnalytics.Core.NBD
{
  public class NBDSolver : ISolver
  {

    public Dictionary<string, double> Train(IEnumerable<ITrainingData> input)
    {
      Dictionary<string, double> result = new Dictionary<string, double>();


      IEnumerable<NBDData> data = input.Cast<NBDData>();
      SolverContext context = SolverContext.GetContext();
      Model model = context.CreateModel();

      Set occurences = new Set(Domain.Any, "occurences");

      Decision r = new Decision(Domain.RealNonnegative, "r");
      Decision alpha = new Decision(Domain.RealNonnegative, "alpha");

      model.AddDecisions(r, alpha);

      Parameter count = new Parameter(Domain.IntegerNonnegative, "count", occurences);
      count.SetBinding(data, "Count", "Occurence");

      model.AddParameter(count);

      Term previous = 0;
      Term cumilative = 0;

      Term[] llArray = (from e in data select GetLogLikelihood(e, r, alpha, ref previous, ref cumilative)).ToArray();


      //r and alpha should not  be equal to zero, and upper bound to max
      model.AddConstraint("rlimits", 0 <= r <= 1000);
      model.AddConstraint("alphalimits", 0 <= alpha <= 1000);

      model.AddGoal("SumLogLikelihood", GoalKind.Maximize, Model.Sum(llArray));

      Solution solution = context.Solve();
      Report report = solution.GetReport();

      if (report.SolutionQuality == SolverQuality.LocalOptimal || report.SolutionQuality == SolverQuality.Optimal)
      {
        foreach (Decision parameter in solution.Decisions)
        {
          result.Add(parameter.Name, parameter.ToDouble());
        }
      }

      return result;
    }


    private Term GetLogLikelihood(NBDData entry, Decision r, Decision alpha, ref Term previous, ref Term cumilative)
    {
      Term p_x = 0;

      foreach (var item in GetOccurenceRange(entry.Occurence))
      {
        if (item == 0)
        {
          p_x = p_x + Model.Power((alpha / (1 + alpha)), r);
        }
        else if (item < 0)
        {
          p_x = p_x + (1 - cumilative);
        }
        else
        {
          p_x = p_x + ((previous * (r + item - 1)) / (item * (alpha + 1)));
        }
      }

      previous = p_x;

      cumilative = previous + cumilative;

      return entry.Count * Model.Log(p_x);
    }

    private IEnumerable<int> GetOccurenceRange(string occurence)
    {
      try
      {
        var rangeOccurence = new List<int>();

        if (int.TryParse(occurence, out int result))
        {
          rangeOccurence.Add(result);
        }
        else if (occurence.Contains("-"))
        {
          var range = occurence.Split('-');

          if (int.TryParse(range[0], out int startRange))
          {
            if (int.TryParse(range[1], out int endRange))
            {
              rangeOccurence = new List<int>(Enumerable.Range(startRange, endRange-startRange));
            }
          }
        }
        else if (occurence.Contains("+"))
        {
          rangeOccurence.Add(-1);
        }

        return rangeOccurence;
      }
      catch (System.Exception)
      {
        throw new System.Exception("Invalid input data");
      }
    }

  }
}
