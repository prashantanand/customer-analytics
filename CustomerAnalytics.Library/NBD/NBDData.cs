﻿namespace CustomerAnalytics.Core.NBD
{
  public class NBDData: ITrainingData
  {
    public string Occurence { get; set; }
    public int Count { get; set; }
  }
}