﻿namespace CustomerAnalytics.Core
{
  public class TrainingResult
  {
    public ResultCode Result { get; set; }
    public string Message { get; set; }
    public object Data { get; set; }
    public IModel Model { get; set; }
  }
}
