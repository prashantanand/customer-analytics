﻿using CustomerAnalytics.Core.NBD;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CustomerAnalytics.Tests
{
  [TestClass]
  public class NBDTests
  {
    [TestMethod]
    public void TestTraining()
    {
      NBDData[] data = new NBDData[]
     {

        new NBDData { Occurence = "0", Count = 1511 },
        new NBDData { Occurence = "1",   Count = 151 },
        new NBDData { Occurence = "2",   Count = 107 },
        new NBDData { Occurence = "3",   Count = 101 },
        new NBDData { Occurence = "4",   Count = 93 },
        new NBDData { Occurence = "5",   Count = 83 },
        new NBDData { Occurence = "6",   Count = 74 },
        new NBDData { Occurence = "7",   Count = 64 },
        new NBDData { Occurence = "8",   Count = 56 },
        new NBDData { Occurence = "9",   Count = 48 },
        new NBDData { Occurence = "10+",   Count = 212 },
     };
      NBDSolver solver = new NBDSolver();
      System.Collections.Generic.Dictionary<string, double> result = solver.Train(data);

      foreach (string item in result.Keys)
      {
        if (item == "R")
        {
          Assert.IsTrue(Math.Round(result[item], 4) == Math.Round(0.18095182550806D, 4), "R is accurate");
        }
        if
          (item == "Alpha")
        {
          Assert.IsTrue(Math.Round(result[item], 2) == Math.Round(0.0590779034207311, 2), "Alpha is accurate");
        }
      }
    }

    [TestMethod]
    public void TestTrainingRange()
    {
      NBDData[] data = new NBDData[]
     {

        new NBDData { Occurence = "0", Count = 1020 },
        new NBDData { Occurence = "1",   Count = 166 },
        new NBDData { Occurence = "2",   Count = 270 },
        new NBDData { Occurence = "3-5",   Count = 279 },
        new NBDData { Occurence = "6+",   Count = 130 },
     };
      NBDSolver solver = new NBDSolver();
      System.Collections.Generic.Dictionary<string, double> result = solver.Train(data);

      foreach (string item in result.Keys)
      {
        if (item == "R")
        {
          Assert.IsTrue(Math.Round(result[item], 4) == Math.Round(0.408724814, 4), "R is accurate");
        }
        if
          (item == "Alpha")
        {
          Assert.IsTrue(Math.Round(result[item], 2) == Math.Round(0.270310398, 2), "Alpha is accurate");
        }
      }
    }

    [TestMethod]
    public void TestSpikeTraining()
    {
      NBDData[] data = new NBDData[]
     {

        new NBDData { Occurence = "0", Count = 1511 },
        new NBDData { Occurence = "1",   Count = 151 },
        new NBDData { Occurence = "2",   Count = 107 },
        new NBDData { Occurence = "3",   Count = 101 },
        new NBDData { Occurence = "4",   Count = 93 },
        new NBDData { Occurence = "5",   Count = 83 },
        new NBDData { Occurence = "6",   Count = 74 },
        new NBDData { Occurence = "7",   Count = 64 },
        new NBDData { Occurence = "8",   Count = 56 },
        new NBDData { Occurence = "9",   Count = 48 },
        new NBDData { Occurence = "10+",   Count = 212 },
     };

      NBDWithSpikeAtZeroSolver solver = new NBDWithSpikeAtZeroSolver();
      System.Collections.Generic.Dictionary<string, double> result = solver.Train(data);

      foreach (string item in result.Keys)
      {
        if (item == "r")
        {
          Assert.IsTrue(Math.Round(result[item], 4) == Math.Round(1.226478309, 4), "R is accurate");
        }
        if (item == "alpha")
        {
          Assert.IsTrue(Math.Round(result[item], 2) == Math.Round(0.215155485, 2), "Alpha is accurate");
        }
      }
    }

  }
}
