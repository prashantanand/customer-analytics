﻿using Accord.Statistics.Testing;
using CustomerAnalytics.Core.NBD;
using ExcelDna.Integration;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Excel.Application;


namespace CustomerAnalytics.Excel
{
  public class NBDDataManager
  {
    public static void WriteSetupData()
    {
      Application xlApp = (Application)ExcelDnaUtil.Application;

      Workbook xlWorkBook = xlApp.ActiveWorkbook;
      if (xlWorkBook == null)
      {
        return;
      }

      Worksheet xlWorkSheet = xlWorkBook.ActiveSheet;

      //Worksheet xlWorkSheet = xlWorkBook.Worksheets.Add(Type: XlSheetType.xlWorksheet);

      xlWorkSheet.Range["A1"].Value = "Occurence";
      xlWorkSheet.Range["B1"].Value = "Counts";

      Range headerRow = xlWorkSheet.Range["A1", "B1"];
      headerRow.Font.Size = 12;
      headerRow.Font.Bold = true;

      xlWorkSheet.Columns["A:B"].EntireColumn.AutoFit();
      xlWorkSheet.Columns["A:A"].EntireColumn.NumberFormat = "@";

      Marshal.ReleaseComObject(xlWorkSheet);
      Marshal.ReleaseComObject(xlWorkBook);
      Marshal.ReleaseComObject(xlApp);
    }

    public static void ReadTrainingData()
    {
      NBDData[] data;

      Application xlApp = (Application)ExcelDnaUtil.Application;

      Workbook xlWorkBook = xlApp.ActiveWorkbook;
      if (xlWorkBook == null)
      {
        return;
      }

      Worksheet xlWorkSheet = xlWorkBook.ActiveSheet;

      Range xlUsedRange = xlWorkSheet.UsedRange;

      if (xlUsedRange.Columns.Count != 2)
      {
        MessageBox.Show("Training data is expected in two columns olnly. Occurence and Count");
        return;
      }

      if (xlUsedRange.Rows.Count <= 1)
      {
        MessageBox.Show("Insufficient data to train. Please enter some values for Occurences and Counts");
        return;
      }

      data = new NBDData[xlUsedRange.Rows.Count - 1].Select(h => new NBDData()).ToArray();

      try
      {
        int index;

        foreach (Range item in xlUsedRange)
        {
          if (item.Row > 1)
          {
            index = item.Row - 2;

            if (item.Column == 1)
            {
              data[index].Occurence = System.Convert.ToString(item.Value2);
            }
            else
            {
              data[index].Count = (int)item.Value2;
            }
          }
        }
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.ToString());
      }

      try
      {
        NBDSolver solver = new NBDSolver();
        System.Collections.Generic.Dictionary<string, double> result = solver.Train(data);

        int index = 0;
        double r, alpha;
        int totalCount = 0;

        double[] px = new double[data.Length];
        double[] expected = new double[data.Length];

        r = result["r"];
        alpha = result["alpha"];
        totalCount = data.Sum(x => x.Count);

        object[,] xlExpected = new object[data.Length, 1];
        object[,] xlLogLikelihood = new object[data.Length, 1];
        object[,] xlProbability = new object[data.Length, 1];

        foreach (NBDData item in data)
        {
          px[index] = 0;
          double previous = 0;

          foreach (var occurence in GetOccurenceRange(item.Occurence))
          {
            if (occurence == 0)
            {
              px[index] = px[index] + Math.Pow((alpha / (1 + alpha)), r);
            }
            else if (occurence < 0)
            {
              px[index] = px[index] + (1 - px.Sum());
            }
            else
            {
              if (previous == 0) previous = px[index - 1];
              previous = ((previous * (r + occurence - 1)) / (occurence * (alpha + 1)));
              px[index] = px[index] + previous;
            }
          }
          
          xlProbability[index, 0] = px[index];

          xlLogLikelihood[index, 0] = item.Count * Math.Log(px[index]);

          expected[index] = px[index] * totalCount;

          xlExpected[index, 0] = expected[index];

          index++;
        }

        xlWorkSheet.WriteArray("Expected", 3, xlExpected);
        xlWorkSheet.WriteArray("Probability", 4, xlProbability);
        xlWorkSheet.WriteArray("LogLikelihood", 5, xlLogLikelihood);
      

        ChiSquareTest chi = new ChiSquareTest(expected, data.Select(x => (double)x.Count).ToArray(), 8);

        double pValue = chi.PValue; // 0.23

        xlWorkSheet.Range["G1"].Value = "Model";
        xlWorkSheet.Range["G2"].Value = "r";
        xlWorkSheet.Range["H2"].Value = result["r"];
        xlWorkSheet.Range["G3"].Value = "alpha";
        xlWorkSheet.Range["H3"].Value = result["alpha"];
        xlWorkSheet.Range["G4"].Value = "p-value";
        xlWorkSheet.Range["H4"].Value = pValue;

        Range xStartCell = (Range)xlWorkSheet.Cells[1, 1];
        Range xEndCell = (Range)xlWorkSheet.Cells[data.Length + 1, 1];

        Range yStartCell = (Range)xlWorkSheet.Cells[1, 2];
        Range yEndCell = (Range)xlWorkSheet.Cells[data.Length + 1, 3];

        DrawChart(xlWorkSheet, xStartCell, xStartCell, yStartCell, yEndCell, "NBD Model Fit", "Occurence", "Count");

        //xlWorkSheet.WriteArray("P(Occurence)", 3, xlProbability);
        //xlWorkSheet.WriteArray("LogLikelihood", 4, xlLogLikelihood);

      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.ToString());
      }

      Marshal.ReleaseComObject(xlWorkSheet);
      Marshal.ReleaseComObject(xlWorkBook);
      Marshal.ReleaseComObject(xlApp);
    }

    private static IEnumerable<int> GetOccurenceRange(string occurence)
    {
      try
      {
        var rangeOccurence = new List<int>();

        if (int.TryParse(occurence, out int result))
        {
          rangeOccurence.Add(result);
        }
        else if (occurence.Contains("-"))
        {
          var range = occurence.Split('-');

          if (int.TryParse(range[0], out int startRange))
          {
            if (int.TryParse(range[1], out int endRange))
            {
              rangeOccurence = new List<int>(Enumerable.Range(startRange, (endRange - startRange)+1));
            }
          }
        }
        else if (occurence.Contains("+"))
        {
          rangeOccurence.Add(-1);
        }

        return rangeOccurence;
      }
      catch (System.Exception)
      {
        throw new System.Exception("Invalid input data");
      }
    }



    private static void DrawChart(Worksheet xlWorkSheet,
                                  object xStartCell,
                                  object xEndCell,
                                  object yStartCell,
                                  object yEndCell,
                                  string graphTitle,
                                  string xAxisTitle,
                                  string yAxisTitle)
    {
      // Add chart.
      ChartObjects charts = xlWorkSheet.ChartObjects() as ChartObjects;
      ChartObject chartObject = charts.Add(60, 10, 300, 300) as ChartObject;
      Chart chart = chartObject.Chart;

      // Set chart properties.
      chart.ChartType = XlChartType.xlColumnClustered;

      chart.ChartWizard(Source: xlWorkSheet.get_Range(xStartCell, yEndCell),
                        Title: graphTitle,
                        CategoryTitle: xAxisTitle,
                        ValueTitle: yAxisTitle);
    }

  }
}
