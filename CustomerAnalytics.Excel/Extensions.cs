﻿using Microsoft.Office.Interop.Excel;

namespace CustomerAnalytics.Excel
{
  public static class Extensions
  {
    public static void WriteArray(this Worksheet xlWorkSheet, string header, int column, object[,] data)
    {
      var headerCell = (Range)xlWorkSheet.Cells[1, column];
      var startCell = (Range)xlWorkSheet.Cells[2, column];
      var endCell = (Range)xlWorkSheet.Cells[2 + data.Length - 1, column];
      var writeRange = xlWorkSheet.Range[startCell, endCell];

      headerCell.Value2 = header;
      headerCell.Font.Bold = true;

      writeRange.Value2 = data;
      writeRange.EntireColumn.AutoFit();
    }
  }
}
