﻿using ExcelDna.Integration.CustomUI;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace CustomerAnalytics.Excel
{

  [ComVisible(true)]
  public class RibbonController : ExcelRibbon
  {
    public override string GetCustomUI(string RibbonID)
    {
      return @"
                <customUI xmlns='http://schemas.microsoft.com/office/2006/01/customui'>
                <ribbon>
                  <tabs>
                    <tab id='rbCUstomerAnalytics' label='Customer Analytics'>
                      <group id='grpNBD' label='NBD'>
                        <button id='btnNBDSetup' label='Setup' onAction='OnNBDSetupClicked'/>
                        <button id='btnNBDTain' label='Train' onAction='OnNBDTrainClicked'/>
                      </group >
                    </tab>
                  </tabs>
                </ribbon>
              </customUI>";
    }

    public void OnNBDSetupClicked(IRibbonControl control)
    {
      NBDDataManager.WriteSetupData();
    }

    public void OnNBDTrainClicked(IRibbonControl control)
    {
      NBDDataManager.ReadTrainingData();
    }

    public void OnNBDSpikeSetupClicked(IRibbonControl control)
    {
      NBDSpikeDataManager.WriteSpikeSetupData();
    }

    public void OnNBDSpikeTrainClicked(IRibbonControl control)
    {
      NBDSpikeDataManager.ReadSpeakTrainingData();
    }

  }
}
